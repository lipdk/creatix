<?php
// DIC configuration

$container = $app->getContainer();

$container['view'] = function ($c) {
	
	$settings = $c->get('settings')['renderer'];
	$view = new \Slim\Views\Twig(__DIR__ . $settings['template_path'], [
		'cache' => false,
		'debug' => true,
	]);
	
	$view->addExtension(new \Slim\Views\TwigExtension(		
		$c->get('router'),		
		$c->get('request')->getUri()		
	));
	
	return $view;
	
};

// view renderer
$container['renderer'] = function ($c) {
	$settings = $c->get('settings')['renderer'];
	return new Slim\Views\PhpRenderer($settings['template_path']);
};

$container['db'] = function ($c) {

	// PDO
	$settings = $c->get('settings')['database'];
	$pdo = new PDO("mysql:host=" . $settings['host'] . ";dbname=" . $settings['dbname'], $settings['user'], $settings['pass']);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	return $pdo;

};