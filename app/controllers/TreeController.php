<?php

namespace App;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Illuminate\Database\Query\Builder;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use PDO;

class TreeController
{
	private $view;
	private $logger;
	protected $table;

	public function __construct()
	{
		//
	}

	public function __invoke(Request $request, Response $response, $args)
	{
		//
	}

	/**
	 * Read JSON and Insert in Database
	 */
	public function insertAll($request, $response, $args, $conn)
	{

		// Read JSON
		$jsonFile = file_get_contents('tree.json');
		if( $jsonFile ) $jsonFile = json_decode($jsonFile);

		if( count($jsonFile) > 0 )
		{

			// Truncate Table before inserts 
			$truncate = $conn->prepare("TRUNCATE TABLE `tree`");
  			$truncate->execute();

  			$rowsInserted = 0;			
			foreach ($jsonFile as $key => $item)
			{

				try
				{
					$conn->beginTransaction();

					$parent = 0;
					if( $item->father != '' ) $parent = $item->father;
					
					$sql = "INSERT INTO tree (
						id,
						name,
						size,
						parent
					)
					VALUES
					(
						:ID, 
						:name, 
						:size, 
						:parent
					)";
											  
					$stmt = $conn->prepare($sql);
												  
					$stmt->bindParam(':ID', $item->id, PDO::PARAM_INT);       
					$stmt->bindParam(':name', $item->title, PDO::PARAM_STR); 
					$stmt->bindParam(':size', $item->size, PDO::PARAM_INT);
					$stmt->bindParam(':parent', $parent, PDO::PARAM_INT); 

					$stmt->execute();
					$rowsInserted++;
					$conn->commit();
				}
				catch (Exception $e){
					$conn->rollback();
					echo $e->getMessage();
				}
			}

			echo 'Rows Inserted: '.$rowsInserted;
			
		}
	}

	/**
	 * Build Tree Recursively
	 */
	public function buildTree($items = array(), $parentID = 0)
	{
	    $branch = array();

	    foreach ($items as $item) {
	        if ($item['parent'] == $parentID) {
	            $children = $this->buildTree($items, $item['id']);
	            if ($children) {
	                $item['children'] = $children;
	            }
	            $branch[] = $item;
	        }
	    }

	    return $branch;
	}	


	/**
	 * Get ALL rows of Table
	 */
	public function getAll($request, $response, $args=array(), $conn, $echo=true)
	{

		$sql = "SELECT * FROM tree";

		if( isset($args['query']) && $args['query'] != '' )
		{
			$sql .= " WHERE name LIKE '%".$args['query']."%'";
		}

		$tree = $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);


		/*
		if( isset($args['query']) && $args['query'] != '' )
		{}
		*/

		$tree = $this->buildTree($tree);
		if( $echo )
			echo json_encode($tree);
		else
			return json_encode($tree);
		
	}


}