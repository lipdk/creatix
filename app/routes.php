<?php


$app->get('/', function ($request, $response, $args)
{
	return $this->view->render($response, 'home.phtml');
});

//$app->get('/tree', '\TreeController:home');
$app->get('/insert', function ($request, $response, $args)
{
	$conn = $this->db;
	$treeController = new \App\TreeController();
	$treeController->insertAll($request, $response, $args, $conn);

});

$app->get('/output', function ($request, $response, $args)
{	
	$conn = $this->db;
	$treeController = new \App\TreeController();
	if($request->getQueryParams()) $args = $request->getQueryParams();
	$treeController->getAll($request, $response, $args, $conn);

	return $response->withHeader(
		'Content-Type',
		'application/json'
	);

});

/*
$app->get('/tree-view', function ($request, $response, $args)
{

	$conn = $this->db;
	$treeController = new \App\TreeController();    
	$tree = $treeController->getAll($request, $response, $args, $conn, false);

	return $this->view->render($response, 'treeView.phtml', array('tree'=>$tree));
	//return $this->renderer->render($response, 'treeView.phtml', $args);
});
*/