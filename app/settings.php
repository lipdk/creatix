<?php
return [
    'settings' => [

        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => true,  // set to false in production
        'addContentLengthHeader' => false,

        // Renderer settings
        'renderer' => [
            'template_path' => '/views/',
        ],

        // database (PDO)
        'database' => [
            'prefix'    => '',
            'host'      => 'localhost',
            'user'      => 'homestead',
            'pass'      => 'secret',
            'dbname'    => 'creatix',
        ],

    ],
];


