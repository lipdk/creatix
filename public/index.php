<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

// Instantiate the app
$settings = require __DIR__ . '/../app/settings.php';

$app = new \Slim\App($settings);

// Setup Controllers
require __DIR__ . '/../app/controllers/TreeController.php';

// Setup dependencies
require __DIR__ . '/../app/dependencies.php';

// Register middleware
require __DIR__ . '/../app/middleware.php';

// Register routes
require __DIR__ . '/../app/routes.php';

// Run app
$app->run();