(function(){

	'use strict';
	
	var treeViewApp = angular.module('treeViewApp', ['ui.tree', 'RecursionHelper']);

	treeViewApp.controller('BigTreeCtrl', ['$scope', '$http', function ($scope, $http)
	{

		$http.get('output').then(function(response){
			$scope.data = response.data;
			$scope.doing_async = false;
		});

		$scope.remove = function (scope) {
			scope.remove();
		};

		$scope.toggle = function (scope) {
			scope.toggle();
		};

		$scope.moveLastToTheBeginning = function () {
			var a = $scope.data.pop();
			$scope.data.splice(0, 0, a);
		};

		$scope.newSubItem = function (scope) {
			var nodeData = scope.$modelValue;
			nodeData.nodes.push({
				id: nodeData.id * 10 + nodeData.nodes.length,
				name: nodeData.name + '.' + (nodeData.nodes.length + 1),
				nodes: []
			});
		};

		$scope.collapseAll = function () {
			$scope.$broadcast('angular-ui-tree:collapse-all');
		};

		$scope.expandAll = function () {
			$scope.$broadcast('angular-ui-tree:expand-all');
		};

		$scope.visible = function (item) {
			return !($scope.query && $scope.query.length > 0
			&& item.name.indexOf($scope.query) == -1);
		};

		$scope.findNodes = function () {};

	}]);

}());