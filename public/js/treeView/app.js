(function(){

	'use strict';
	
	var treeViewApp = angular.module('treeViewApp', ['angularBootstrapNavTree']);

	treeViewApp.controller('BigTreeCtrl', ['$scope', '$http', '$filter', '$timeout', function ($scope, $http, $filter, $timeout)
	{

		// init vars
		var treeView;
		$scope.treeView = treeView = {};		
		$scope.data = [];
		$scope.doing_async = true;
		$scope.search;

		// load data from JSON
		$http.get('output').then(function(response){
			$scope.data = response.data;
			$scope.doing_async = false;
		});

		// search input. Send params to backend.
		var searchTermTimeout;
		$scope.$watch('search', function() {
			if (searchTermTimeout) $timeout.cancel(searchTermTimeout);
			searchTermTimeout = $timeout(function() {			
				if( $scope.search && $scope.search != '' )
				{
					$scope.doing_async = true;
					$http.get('output', {
						params: {
							query: $scope.search
						}
					}).then(function(response){
						$scope.data = response.data;
						$scope.doing_async = false;
					});
				}
			}, 250); // delay 250 ms
		});

	}]);




}());