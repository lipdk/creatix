# Creatix Test 

> Feel free to use any tools, frameworks or libraries. Whatever you are most comfortable with or something new that you wanted to try for a long time. Just let us know what you chose, why, and what was your previous experience with it.

In this test, i used Sublime Text, Git Bash, NodeJS (NPM), Bower and Composer to develop and manage dependencies.

I chose jQuery to build the JSON, i think it's better to manipulate DOM. I chose PHP to implement the back-end and MySQL as database. I used Slim PHP as framework,  is simple and organized, i thought should be good for this project.

I used Angular and Bootstrap (CSS) in the view.

http://stack-felipe515437.codeanyapp.com/creatix/public/

[Create JSON from ul/li structure](http://stack-felipe515437.codeanyapp.com/creatix/public/tree.html)
[JSON Example](http://stack-felipe515437.codeanyapp.com/creatix/public/tree.json)
[Read JSON and insert in MySQL Database](http://stack-felipe515437.codeanyapp.com/creatix/public/insert)
[Export JSON from MySQL Database](http://stack-felipe515437.codeanyapp.com/creatix/public/output)
[Data View](http://stack-felipe515437.codeanyapp.com/creatix/public/treeView.html)

> Step 2) Can you write an algrorithm that will output such a tree? No cheating here, you have to read this data in a linear form from the database.

Reply: Yes, it is in /app/controllers/TreeController.php. I used recursive function to build the nested tree

> Step 2) What is the complexity of your algorithm (in big O notation)?

Reply: In Worst case, O(n2).

> Step 3) Can you design and build an interface to show this data?

Reply: http://stack-felipe515437.codeanyapp.com/creatix/public/treeView.html

> Can you implement search in this UI?

Reply: I tried (: in 9h of development, i could not find a good way to implement a search. I tried in angular, but is slow and difficult to find all the parents and show in tree view. In back-end, the problem is similar. 
The best way should be insert a reference in database, like 1.3.2.11, and after the search, build the tree again based on these reference.

### Install Dependencies

```
$ bower install
$ composer install
```
